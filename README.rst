====
REST
====

About REST
----------

REST is to write blog entry using reStructureText (rst) and
a repository. No database interaction. Everything is static.

The security strength of REST is entirely on the user. To
publish local content to a remote server, the user must
supply the credential through the command-line.

In the future release, we can consider a one-file deployment
to upload a single RST to the remote server without the entire
blog repository to exist on the disk. Instead, we can render 
everything on the server. However, this feature is not situable
for Bitbucket and GitHub users as static pages are just normal
repoistory. There is no server environment for this feature to
work.


Stage I
-------

Code a sample template using Bootstrap. We do this first because
I am not good at CSS/HTML.

Stage II
--------

Write the core to publish RST, including a basic template.


Stage III
---------

Make it into a Debian package.


